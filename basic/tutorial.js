const model = tf.sequential();

model.add(tf.layers.dense({units: 1, inputShape:[1]}))

model.add(tf.layers.dense({units: 64, inputShape:[1]}))

model.add(tf.layers.dense({units: 1, inputShape:[64]}));

model.compile({loss: "meanSquaredError", optimizer:"sgd"});

const xs = [1,2,3,4,5];
const ys = [2,4,6,8,10];

model.fit(tf.tensor2d(xs), tf.tensor2d(ys));

model.fit(tf.tensor(xs), tf.tensor(ys));

model.fit(tf.tensor2d(xs, [5, 1]), tf.tensor2d(ys, [5, 1]));

model.predict(tf.tensor2d([6], [1, 1]));
model.predict(tf.tensor2d([6], [1, 1])).print();
model.predict(tf.tensor2d([6], [1, 1])).dataSync();

model.fit(tf.tensor2d(xs, [5, 1]), tf.tensor2d(ys, [5, 1]), {epochs:150, shuffle: true});
model.fit(tf.tensor2d(xs, [5, 1]), tf.tensor2d(ys, [5, 1]), {epochs:1500, shuffle: true});

model.predict(tf.tensor2d([6], [1, 1]));
model.predict(tf.tensor2d([20], [1, 1]));
model.predict(tf.tensor2d([200], [1, 1]));
